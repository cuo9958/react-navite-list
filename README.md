# react-navite-list

#### 项目介绍
list是一个可以加载超级长的图片列表.

#### 继承
组件基于FlatList开发,在加载超长的图片和文字列表的情况下可以做到不闪退不白屏.

#### 安装教程

1. 引用index.js
2. 使用组件
```javascript
/**
 * 自定义列表项
 */
class CusItem extends Item {
    render() {
        const { item } = this.props;
        return <View onLayout={e => this.props.onLayout(e)} style={itemStyle.box}>
            <Image style={itemStyle.img} source={{ uri: item.show ? item.image : img }} />
            <Text style={itemStyle.tit} numberOfLines={1}>{item.goodsName}</Text>
            <Text style={itemStyle.txt} numberOfLines={1}>{item.goodsShowDesc}</Text>
        </View>
    }
}
/**
 * 自定义列表
 */
class CusList extends List {

    /**
     * 自定义初始化方法
     */
    init() {
        console.log("初始化完成")
    }
    /**
     * 自定义加载远程数据
     * 返回data.currPageNo  当前页
     * 返回data.items       列表
     * 返回data.totalPages  总页数
     * 返回data.totalCount  总个数
     * 返回data.startIndex  页序号
     */
    async load(pageIndex) {
        console.log("加载数据")
        try {
            let res = await fetch("https://dalingjia.com/xc_sale/goods/list.do?start=" + pageIndex);
            let data = await res.json();
            if (data.data) data = data.data;
            console.log(pageIndex, data);
            return data;
        } catch (error) {
            console.log(error.message);
            return null;
        }
    }

    renderItem(item, index) {
        return <CusItem show={item.show} item={item} />
    }
}
```