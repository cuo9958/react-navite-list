/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image,
    Dimensions
} from 'react-native';
import List from "./List"
import Item from "./Item"

const deviceWidth = Dimensions.get('window').width;
const img = require("./default_img")

/**
 * 自定义列表项
 */
class CusItem extends Item {
    render() {
        const { item } = this.props;
        return <View onLayout={e => this.props.onLayout(e)} style={itemStyle.box}>
            <Image style={itemStyle.img} source={{ uri: item.show ? item.image : img }} />
            <Text style={itemStyle.tit} numberOfLines={1}>{item.goodsName}</Text>
            <Text style={itemStyle.txt} numberOfLines={1}>{item.goodsShowDesc}</Text>
        </View>
    }
}
/**
 * 自定义列表
 */
class CusList extends List {

    /**
     * 自定义初始化方法
     */
    init() {
        console.log("初始化完成")
    }
    /**
     * 自定义加载远程数据
     * 返回data.currPageNo  当前页
     * 返回data.items       列表
     * 返回data.totalPages  总页数
     * 返回data.totalCount  总个数
     * 返回data.startIndex  页序号
     */
    async load(pageIndex) {
        console.log("加载数据")
        try {
            let res = await fetch("https://dalingjia.com/xc_sale/goods/list.do?start=" + pageIndex);
            let data = await res.json();
            if (data.data) data = data.data;
            console.log(pageIndex, data);
            return data;
        } catch (error) {
            console.log(error.message);
            return null;
        }
    }

    renderItem(item, index) {
        return <CusItem show={item.show} item={item} />
    }
}

export default class App extends Component {
    render() {
        return <View style={{ flex: 1 }}>
            <Text style={{ height: 40 }}>测试超长列表</Text>
            <CusList show={true} />
        </View>;
    }
}

const itemStyle = StyleSheet.create({
    box: {
        backgroundColor: "#fff",
        marginBottom: 10,
        height: 250
    },
    img: {
        width: deviceWidth, height: 200
    },
    tit: {
        fontSize: 16,
        marginTop: 5,
        marginBottom: 5
    },
    txt: {
        fontSize: 12,
        marginTop: 5,
        marginBottom: 5
    }
})